package com.tazkia.moodlesmile.dto;

import com.sun.istack.NotNull;
import lombok.Data;

import java.math.BigInteger;

@Data
public class MdlUserDto {

    private BigInteger id;
    private String auth;
    private Integer confirmed;
    private Integer policyagreed;
    private Integer deleted;
    private Integer suspended;
    private BigInteger mnethostid;
    private String username;
    private String password;
    private String idnumber;
    private String firstname;
    private String lastname;
    private String email;
    private Integer emailstop;
    private String icq;
    private String skype;
    private String yahoo;
    private String aim;
    private String msn;
    private String phone1;
    private String phone2;
    private String institution;
    private String department;
    private String address;
    private String city;
    private String country;
    private String lang;
    private String calendartype;
    private String theme;
    private String timezone;
    private BigInteger firstaccess;
    private BigInteger lastaccess;
    private BigInteger lastlogin;
    private BigInteger currentlogin;
    private String lastip;
    private String secret;
    private BigInteger picture;
    private String url;
    private String description;
    private Integer descriptionformat;
    private Integer mailformat;
    private Integer maildigest;
    private Integer maildisplay;
    private Integer autosubscribe;
    private Integer trackforums;
    private BigInteger timecreated;
    private BigInteger timemodified;
    private BigInteger trustbitmask;
    private String imagealt;
    private String lastnamephonetic;
    private String firstnamephonetic;
    private String middlename;
    private String alternatename;
    private String moodlenetprofile;

}
