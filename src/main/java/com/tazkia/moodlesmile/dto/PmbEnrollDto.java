package com.tazkia.moodlesmile.dto;

import lombok.Data;

@Data
public class PmbEnrollDto {
    private String nomor;
    private String nama;
    private String email;
    private String noHp;
    private String idElearning;
    private String status;
}
