package com.tazkia.moodlesmile.service;

import com.tazkia.moodlesmile.dao.MdlKrsSmileDao;
import com.tazkia.moodlesmile.dao.MdlRoleAssignmentsDao;
import com.tazkia.moodlesmile.dao.MdlUserEnrolmentsDao;
import com.tazkia.moodlesmile.dto.EnrollMahasiswaDto;
//import com.tazkia.moodlesmile.entity.MdlKrsSmile;
import com.tazkia.moodlesmile.entity.MdlKrsSmile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

@Service
@EnableScheduling
public class StudentService {

    @Autowired
    private MdlKrsSmileDao mdlKrsSmileDao;

    @Autowired
    private MdlUserEnrolmentsDao mdlUserEnrolmentsDao;

    @Autowired
    private MdlRoleAssignmentsDao mdlRoleAssignmentsDao;

    WebClient webClient1 = WebClient.builder()
            .baseUrl("https://smile.tazkia.ac.id")
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .build();

    public List<EnrollMahasiswaDto> getEnrollStudent() {
        return webClient1.get()
                .uri("/api/enrollKrsMahasiswa")
                .retrieve().bodyToFlux(EnrollMahasiswaDto.class)
                .collectList()
                .block();
    }


    @Scheduled(cron = "0 0 01 * * ? ", zone = "Asia/Jakarta")
    public void enrollStudent(){


        List<EnrollMahasiswaDto> listEnrollStudent = getEnrollStudent();

        for(EnrollMahasiswaDto emd : listEnrollStudent){
            System.out.println(emd.getIdNumberElearning());
            System.out.println("PROGRESS ENROLL STUDENT : START");

            MdlKrsSmile mks = new MdlKrsSmile();

            mks.setIdNumberElearning(emd.getIdNumberElearning());
            mks.setIdKrs(emd.getIdKrs());
            mks.setIdKrsDetail(emd.getIdKrsDetail());
            mks.setIdJadwal(emd.getIdJadwal());
            mks.setIdMahasiswa(emd.getIdMahasiswa());
            mks.setEmail(emd.getEmail());
            mks.setNim(emd.getNim());
            mdlKrsSmileDao.save(mks);

        }

        System.out.println("PROGRESS ENROLL STUDENT : DONE");
        System.out.println("==============================");

        System.out.println("PROGRESS ADD USER ENROL : START");
        mdlUserEnrolmentsDao.userEnrollSmile();
        System.out.println("PROGRESS ADD USER ENROL : FINISH");

        System.out.println("PROGRESS ADD ROLE ENROL : START");
        mdlRoleAssignmentsDao.studentAssignment();
        System.out.println("PROGRESS ADD ROLE ENROL : FINISH");

    }


}
