package com.tazkia.moodlesmile.dao;

import com.tazkia.moodlesmile.dto.MdlAttendanceLogDosenIntDto;
import com.tazkia.moodlesmile.dto.MdlAttendanceLogMahasiswaIntDto;
import com.tazkia.moodlesmile.entity.MdlAttendanceLog;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

public interface MdlAttendanceLogDao extends PagingAndSortingRepository<MdlAttendanceLog, BigInteger> {

//    List<MdlAttendanceLog> findBySessionidAttendanceidCourseIdnumberAndIpaddressIsNotNull(List<MdlCourse> mdlCourse);0

    @Modifying
    @Query(value = "update mdl_attendance_log set statusimport = 'Berhasil' where sessionid = ?1", nativeQuery = true)
    void updateMdlLog(BigInteger id);

    //find dosen
    @Query(value = "SELECT b.id AS id, 'f635cb68-3da5-4e11-8213-7020636034a2' AS idTahunAkademik,e.fullname AS namaMatakuliah, e.idnumber AS idJadwal, FROM_UNIXTIME(b.sessdate, '%Y-%m-%d') AS waktuMasuk, \n" +
            "FROM_UNIXTIME(b.sessdate, '%Y-%m-%d') AS waktuSelesai,FROM_UNIXTIME(a.timetaken, '%Y-%m-%d') as tanggalInput,'HADIR' AS statusPresensi, 'AKTIF' AS status, f.email AS idDosen, b.description AS beritaAcara, a.id as idLog\n" +
            "FROM mdl_attendance_log AS a \n" +
            "INNER JOIN mdl_attendance_sessions AS b ON a.sessionid = b.id\n" +
            "INNER JOIN mdl_attendance AS c ON b.attendanceid = c.id \n" +
            "INNER JOIN mdl_attendance_statuses AS d ON c.id = d.attendanceid AND a.statusid = d.id\n" +
            "INNER JOIN mdl_course AS e ON c.course = e.id\n" +
            "INNER JOIN mdl_user AS f ON a.takenby = f.id\n" +
            "INNER JOIN mdl_user AS g ON a.studentid = g.id\n" +
            "WHERE e.fullname LIKE '%20231%' AND a.statusimport is null AND FROM_UNIXTIME(b.sessdate, '%Y-%m-%d') = ?1 \n" +
            "GROUP BY b.id", nativeQuery = true)
    List<MdlAttendanceLogDosenIntDto> findJadwalSekarangDosen(LocalDate tanggalImport);

    @Query(value = "SELECT b.id AS id, 'f635cb68-3da5-4e11-8213-7020636034a2' AS idTahunAkademik,e.fullname AS namaMatakuliah, e.idnumber AS idJadwal, FROM_UNIXTIME(b.sessdate, '%Y-%m-%d') AS waktuMasuk, \n" +
            "FROM_UNIXTIME(b.sessdate, '%Y-%m-%d') AS waktuSelesai,FROM_UNIXTIME(a.timetaken, '%Y-%m-%d') as tanggalInput,'HADIR' AS statusPresensi, 'AKTIF' AS status, f.email AS idDosen, b.description AS beritaAcara, a.id as idLog\n" +
            "FROM mdl_attendance_log AS a \n" +
            "INNER JOIN mdl_attendance_sessions AS b ON a.sessionid = b.id\n" +
            "INNER JOIN mdl_attendance AS c ON b.attendanceid = c.id \n" +
            "INNER JOIN mdl_attendance_statuses AS d ON c.id = d.attendanceid AND a.statusid = d.id\n" +
            "INNER JOIN mdl_course AS e ON c.course = e.id\n" +
            "INNER JOIN mdl_user AS f ON a.takenby = f.id\n" +
            "INNER JOIN mdl_user AS g ON a.studentid = g.id\n" +
            "WHERE e.fullname LIKE '%20231%' AND a.statusimport is null AND FROM_UNIXTIME(b.sessdate, '%Y-%m-%d') AND trim(e.idnumber) = ?1 \n" +
            "GROUP BY b.id", nativeQuery = true)
    List<MdlAttendanceLogDosenIntDto> findJadwalSekarangDosen3(String jadwal);


//    @Query(value = "SELECT b.id AS id, 'ac02591c-2f3a-498e-9a91-404e96294d9d' AS idTahunAkademik,e.fullname AS namaMatakuliah, e.idnumber AS idJadwal, FROM_UNIXTIME(b.sessdate, '%Y-%m-%d') AS waktuMasuk, \n" +
//            "FROM_UNIXTIME(b.sessdate, '%Y-%m-%d') AS waktuSelesai,FROM_UNIXTIME(a.timetaken, '%Y-%m-%d') as tanggalInput,'HADIR' AS statusPresensi, 'AKTIF' AS status, f.email AS idDosen, b.description AS beritaAcara, a.id as idLog\n" +
//            "FROM mdl_attendance_log AS a \n" +
//            "INNER JOIN mdl_attendance_sessions AS b ON a.sessionid = b.id\n" +
//            "INNER JOIN mdl_attendance AS c ON b.attendanceid = c.id \n" +
//            "INNER JOIN mdl_attendance_statuses AS d ON c.id = d.attendanceid AND a.statusid = d.id\n" +
//            "INNER JOIN mdl_course AS e ON c.course = e.id\n" +
//            "INNER JOIN mdl_user AS f ON b.lasttakenby = f.id\n" +
//            "INNER JOIN mdl_user AS g ON a.studentid = g.id\n" +
//            "WHERE e.fullname LIKE '%20202%' AND LENGTH(e.idnumber)= 36 AND FROM_UNIXTIME(b.sessdate, '%Y-%m-%d %h:%m:%s')\n" +
//            "GROUP BY b.id", nativeQuery = true)
//    List<MdlAttendanceLogDosenIntDto> findJadwalSekarangDosen2();


    //sds
//    @Query(value = "SELECT b.id AS id, 'ac07497d-0133-445e-af37-9c268df8afd5' AS idTahunAkademik,e.fullname AS namaMatakuliah, e.idnumber AS idJadwal, FROM_UNIXTIME(b.sessdate, '%Y-%m-%d') AS waktuMasuk, \n" +
//            "FROM_UNIXTIME(b.sessdate, '%Y-%m-%d') AS waktuSelesai,FROM_UNIXTIME(a.timetaken, '%Y-%m-%d') as tanggalInput,'HADIR' AS statusPresensi, 'AKTIF' AS status, f.email AS idDosen, b.description AS beritaAcara, a.id as idLog\n" +
//            "FROM mdl_attendance_log AS a \n" +
//            "INNER JOIN mdl_attendance_sessions AS b ON a.sessionid = b.id\n" +
//            "INNER JOIN mdl_attendance AS c ON b.attendanceid = c.id \n" +
//            "INNER JOIN mdl_attendance_statuses AS d ON c.id = d.attendanceid AND a.statusid = d.id\n" +
//            "INNER JOIN mdl_course AS e ON c.course = e.id\n" +
//            "INNER JOIN mdl_user AS f ON b.lasttakenby = f.id\n" +
//            "INNER JOIN mdl_user AS g ON a.studentid = g.id\n" +
//            "WHERE e.fullname LIKE '%20201%' AND FROM_UNIXTIME(b.sessdate, '%Y-%m-%d %h:%m:%s') AND e.idnumber = '7e94f3e0-0a02-11eb-9fd2-76de2a7fe195'\n" +
//            "AND c.name like '%20202%'\n" +
//            "GROUP BY b.id;", nativeQuery = true)
//    List<MdlAttendanceLogDosenIntDto> findJadwalSekarangDosen2();

    @Query(value = "SELECT b.id AS id, 'f635cb68-3da5-4e11-8213-7020636034a2' AS idTahunAkademik,e.fullname AS namaMatakuliah, e.idnumber AS idJadwal, FROM_UNIXTIME(b.sessdate, '%Y-%m-%d') AS waktuMasuk, \n" +
            "FROM_UNIXTIME(b.sessdate, '%Y-%m-%d') AS waktuSelesai,FROM_UNIXTIME(a.timetaken, '%Y-%m-%d') as tanggalInput,'HADIR' AS statusPresensi, 'AKTIF' AS status, f.email AS idDosen, b.description AS beritaAcara, a.id as idLog\n" +
            "FROM mdl_attendance_log AS a \n" +
            "INNER JOIN mdl_attendance_sessions AS b ON a.sessionid = b.id\n" +
            "INNER JOIN mdl_attendance AS c ON b.attendanceid = c.id \n" +
            "INNER JOIN mdl_attendance_statuses AS d ON c.id = d.attendanceid AND a.statusid = d.id\n" +
            "INNER JOIN mdl_course AS e ON c.course = e.id\n" +
            "INNER JOIN mdl_user AS f ON a.takenby = f.id\n" +
            "INNER JOIN mdl_user AS g ON a.studentid = g.id\n" +
            "WHERE e.fullname LIKE '%20231%' AND a.statusimport is null AND FROM_UNIXTIME(b.sessdate, '%Y-%m-%d %h:%m:%s')\n" +
            "GROUP BY b.id;", nativeQuery = true)
    List<MdlAttendanceLogDosenIntDto> findJadwalSekarangDosen2();


//    @Query(value = "SELECT b.id AS id, 'ac02591c-2f3a-498e-9a91-404e96294d9d' AS idTahunAkademik,e.fullname AS namaMatakuliah, e.shortname AS idJadwal, FROM_UNIXTIME(b.sessdate, '%Y-%m-%d') AS waktuMasuk, \n" +
//            "FROM_UNIXTIME(b.sessdate, '%Y-%m-%d') AS waktuSelesai,'HADIR' AS statusPresensi, 'AKTIF' AS status, f.email AS idDosen, b.description AS beritaAcara, a.id as idLog\n" +
//            "FROM mdl_attendance_log AS a \n" +
//            "INNER JOIN mdl_attendance_sessions AS b ON a.sessionid = b.id\n" +
//            "INNER JOIN mdl_attendance AS c ON b.attendanceid = c.id \n" +
//            "INNER JOIN mdl_attendance_statuses AS d ON c.id = d.attendanceid AND a.statusid = d.id\n" +
//            "INNER JOIN mdl_course AS e ON c.course = e.id\n" +
//            "INNER JOIN mdl_user AS f ON b.lasttakenby = f.id\n" +
//            "INNER JOIN mdl_user AS g ON a.studentid = g.id\n" +
//            "WHERE e.fullname LIKE '%20202%' AND LENGTH(e.shortname)= 36 AND a.statusimport is null and e.shortname = 'f71456f4-0868-11eb-9fd2-76de2a7fe195' \n" +
//            "GROUP BY b.id", nativeQuery = true)
//    List<MdlAttendanceLogDosenIntDto> findJadwalSekarangDosen2();


    @Query(value = "SELECT b.id AS id, 'f635cb68-3da5-4e11-8213-7020636034a2' AS idTahunAkademik,e.fullname AS namaMatakuliah, e.idnumber AS idJadwal, FROM_UNIXTIME(a.timetaken, '%Y-%m-%d %h:%m:%s') AS waktuMasuk, \n" +
            "FROM_UNIXTIME(e.enddate, '%Y-%m-%d %h:%m:%s') AS waktuSelesai,d.description AS statusPresensi, 'AKTIF' AS status, g.email AS mahasiswa\n" +
            "FROM mdl_attendance_log AS a \n" +
            "INNER JOIN mdl_attendance_sessions AS b ON a.sessionid = b.id\n" +
            "INNER JOIN mdl_attendance AS c ON b.attendanceid = c.id \n" +
            "INNER JOIN mdl_attendance_statuses AS d ON c.id = d.attendanceid AND a.st  atusid = d.id\n" +
            "INNER JOIN mdl_course AS e ON c.course = e.id\n" +
            "INNER JOIN mdl_user AS f ON a.takenby = f.id\n" +
            "INNER JOIN mdl_user AS g ON a.studentid = g.id\n" +
            "WHERE e.fullname LIKE '%20231%' and e.fullname NOT LIKE '%20201%' AND LENGTH(e.idnumber)= 36 AND FROM_UNIXTIME(b.sessdate, '%Y-%m-%d %h:%m:%s')", nativeQuery = true)
    List<MdlAttendanceLogMahasiswaIntDto> findJadwalSekarangMahasiswa();

    @Query(value = "SELECT b.id AS id, 'f635cb68-3da5-4e11-8213-7020636034a2' AS idTahunAkademik,e.fullname AS namaMatakuliah, e.idnumber AS idJadwal, FROM_UNIXTIME(b.sessdate, '%Y-%m-%d %h:%m:%s') AS waktuMasuk, \n" +
            "FROM_UNIXTIME(b.sessdate, '%Y-%m-%d %h:%m:%s') AS waktuSelesai,d.description AS statusPresensi, 'AKTIF' AS status, g.email AS mahasiswa, g.idnumber as nim\n" +
            "FROM mdl_attendance_log AS a \n" +
            "INNER JOIN mdl_attendance_sessions AS b ON a.sessionid = b.id\n" +
            "INNER JOIN mdl_attendance AS c ON b.attendanceid = c.id \n" +
            "INNER JOIN mdl_attendance_statuses AS d ON c.id = d.attendanceid AND a.statusid = d.id\n" +
            "INNER JOIN mdl_course AS e ON c.course = e.id\n" +
            "INNER JOIN mdl_user AS f ON a.takenby = f.id\n" +
            "INNER JOIN mdl_user AS g ON a.studentid = g.id\n" +
            "WHERE e.fullname LIKE '%20231%' AND a.statusimport is null and b.id= ?1", nativeQuery = true)
    List<MdlAttendanceLogMahasiswaIntDto> findJadwalSekarangMahasiswa2(String id);






}
