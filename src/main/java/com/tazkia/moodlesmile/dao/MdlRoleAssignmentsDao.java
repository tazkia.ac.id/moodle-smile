package com.tazkia.moodlesmile.dao;

import com.tazkia.moodlesmile.entity.MdlRoleAssignments;
import com.tazkia.moodlesmile.entity.MdlUserEnrolments;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigInteger;
import java.util.List;

public interface MdlRoleAssignmentsDao extends PagingAndSortingRepository<MdlRoleAssignments, BigInteger> {


    @Modifying
    @Query(value = "INSERT INTO mdl_role_assignments (roleid, contextid, userid, timemodified, modifierid, component, itemid, sortorder, status_impor)\n" +
            "(SELECT 5 AS roleid, b.id AS contexid, userid, UNIX_TIMESTAMP(NOW()) AS timemodified, 930 AS modifierid,'' AS component,0 AS itemid,0 AS sortorder, 'pmbelearning' AS status_impor FROM\n" +
            "(SELECT a.id_course, a.userid FROM\n" +
            "(SELECT a.id AS id_course, c.userid FROM mdl_course AS a\n" +
            "INNER JOIN mdl_enrol AS b ON a.id = b.courseid\n" +
            "INNER JOIN mdl_user_enrolments AS c ON b.id = c.enrolid\n" +
            "WHERE a.fullname LIKE '%20231%' AND b.enrol = 'manual' AND status_impor = 'pmbelearning')a\n" +
            "LEFT JOIN\n" +
            "(SELECT c.id AS id_course, a.userid FROM mdl_role_assignments AS a\n" +
            "INNER JOIN mdl_context AS b ON a.contextid = b.id\n" +
            "INNER JOIN mdl_course AS c ON b.instanceid = c.id\n" +
            "WHERE c.fullname LIKE '%20231%' AND b.contextlevel = '50') b ON a.id_course = b.id_course AND a.userid = b.userid\n" +
            "WHERE b.id_course IS NULL\n" +
            "GROUP BY a.id_course,a.userid)a\n" +
            "INNER JOIN mdl_context AS b ON a.id_course = b.instanceid\n" +
            "ORDER BY b.id)", nativeQuery = true)
    void roleEnroll();


    @Modifying
    @Query(value = "INSERT INTO mdl_role_assignments (roleid, contextid, userid, timemodified, modifierid, component, itemid, sortorder, status_impor)\n" +
            "(SELECT 5 AS roleid, b.id AS contexid, userid, UNIX_TIMESTAMP(NOW()) AS timemodified, 930 AS modifierid,'' AS component,0 AS itemid,0 AS sortorder, 'yogismile' AS status_impor FROM\n" +
            "(SELECT a.id_course, a.userid FROM\n" +
            "(SELECT a.id AS id_course, c.userid FROM mdl_course AS a\n" +
            "INNER JOIN mdl_enrol AS b ON a.id = b.courseid\n" +
            "INNER JOIN mdl_user_enrolments AS c ON b.id = c.enrolid\n" +
            "WHERE a.fullname LIKE '%20231%' AND b.enrol = 'manual' AND status_impor = 'yogismile')a\n" +
            "LEFT JOIN\n" +
            "(SELECT c.id AS id_course, a.userid FROM mdl_role_assignments AS a\n" +
            "INNER JOIN mdl_context AS b ON a.contextid = b.id\n" +
            "INNER JOIN mdl_course AS c ON b.instanceid = c.id\n" +
            "WHERE c.fullname LIKE '%20231%' AND b.contextlevel = '50') b ON a.id_course = b.id_course AND a.userid = b.userid\n" +
            "WHERE b.id_course IS NULL\n" +
            "GROUP BY a.id_course,a.userid)a\n" +
            "INNER JOIN mdl_context AS b ON a.id_course = b.instanceid\n" +
            "ORDER BY b.id);", nativeQuery = true)
    void studentAssignment();


}
