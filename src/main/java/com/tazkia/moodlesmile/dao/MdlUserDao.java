package com.tazkia.moodlesmile.dao;

import com.tazkia.moodlesmile.entity.MdlUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MdlUserDao extends PagingAndSortingRepository<MdlUser,String>, CrudRepository<MdlUser,String> {
    @Query(value = "SELECT max(id) FROM moodle.mdl_user", nativeQuery = true)
    Integer cariMaxId();

    MdlUser findByIdnumber(String idnumber);

}
