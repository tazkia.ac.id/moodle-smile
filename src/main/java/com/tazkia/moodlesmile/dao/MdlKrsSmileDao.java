package com.tazkia.moodlesmile.dao;

//import com.tazkia.moodlesmile.entity.MdlKrsSmile;

import com.tazkia.moodlesmile.entity.MdlKrsSmile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MdlKrsSmileDao extends PagingAndSortingRepository<MdlKrsSmile,String>, CrudRepository<MdlKrsSmile,String> {
}
