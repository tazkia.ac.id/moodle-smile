package com.tazkia.moodlesmile.dao;

import com.tazkia.moodlesmile.entity.MdlUserEnrolments;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

public interface MdlUserEnrolmentsDao extends PagingAndSortingRepository<MdlUserEnrolments, BigInteger> {


    @Modifying
    @Query(value = "INSERT INTO mdl_user_enrolments (STATUS,enrolid,userid,timestart,timeend,modifierid,timecreated,timemodified,status_impor)\n" +
            "(SELECT aa.* FROM\n" +
            "(SELECT 0 AS STATUS,c.id AS enrolid,d.id AS userid, UNIX_TIMESTAMP(NOW()) AS timestart, 0 AS timeend,  930 AS modifierid, UNIX_TIMESTAMP(NOW()) AS timecreated,\n" +
            "UNIX_TIMESTAMP(NOW()) AS timemodified, 'pmbelearning' AS status_impor\n" +
            "FROM mdl_pmb_enrollment AS a\n" +
            "INNER JOIN mdl_course AS b ON a.id_number_elearning = b.idnumber\n" +
            "INNER JOIN mdl_enrol AS c ON c.courseid = b.id \n" +
            "INNER JOIN mdl_user AS d ON a.email = d.email OR a.id_mahasiswa = d.username OR a.email = d.username\n" +
            "WHERE c.enrol = 'manual' GROUP BY enrolid,userid ORDER BY userid)aa\n" +
            "LEFT JOIN mdl_user_enrolments AS bb ON aa.enrolid = bb.enrolid AND aa.userid = bb.userid \n" +
            "WHERE bb.enrolid IS NULL)", nativeQuery = true)
    void userEnroll();


    @Modifying
    @Query(value = "INSERT INTO mdl_user_enrolments (STATUS,enrolid,userid,timestart,timeend,modifierid,timecreated,timemodified,status_impor)\n" +
            "(SELECT aa.* FROM\n" +
            "(SELECT 0 AS STATUS,c.id AS enrolid,d.id AS userid, UNIX_TIMESTAMP(NOW()) AS timestart, 0 AS timeend,  930 AS modifierid, UNIX_TIMESTAMP(NOW()) AS timecreated,\n" +
            "UNIX_TIMESTAMP(NOW()) AS timemodified, 'yogismile' AS status_impor\n" +
            "FROM mdl_krs_smile AS a\n" +
            "INNER JOIN mdl_course AS b ON a.id_number_elearning = b.idnumber\n" +
            "INNER JOIN mdl_enrol AS c ON c.courseid = b.id \n" +
            "INNER JOIN mdl_user AS d ON a.email = d.email OR a.id_mahasiswa = d.username OR a.email = d.username OR a.nim = d.username\n" +
            "WHERE c.enrol = 'manual' GROUP BY enrolid,userid ORDER BY userid)aa\n" +
            "LEFT JOIN mdl_user_enrolments AS bb ON aa.enrolid = bb.enrolid AND aa.userid = bb.userid \n" +
            "WHERE bb.enrolid IS NULL)", nativeQuery = true)
    void userEnrollSmile();


}
