package com.tazkia.moodlesmile.dao;


import com.tazkia.moodlesmile.entity.MdlPmbEnrollment;
import com.tazkia.moodlesmile.entity.MdlUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MdlPmbEnrollmentDao extends PagingAndSortingRepository<MdlPmbEnrollment,String>, CrudRepository<MdlPmbEnrollment,String> {

}
