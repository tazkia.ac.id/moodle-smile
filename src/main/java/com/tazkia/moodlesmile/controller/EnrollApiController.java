package com.tazkia.moodlesmile.controller;


import com.tazkia.moodlesmile.dao.MdlPmbEnrollmentDao;
import com.tazkia.moodlesmile.dao.MdlRoleAssignmentsDao;
import com.tazkia.moodlesmile.dao.MdlUserDao;
import com.tazkia.moodlesmile.dao.MdlUserEnrolmentsDao;
import com.tazkia.moodlesmile.dto.MdlUserDto;
import com.tazkia.moodlesmile.dto.PmbEnrollDto;
import com.tazkia.moodlesmile.dto.response.BaseResponse;
import com.tazkia.moodlesmile.entity.MdlPmbEnrollment;
import com.tazkia.moodlesmile.entity.MdlUser;
import com.tazkia.moodlesmile.entity.MdlUserEnrolments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
public class EnrollApiController {

    @Autowired
    private MdlUserDao mdlUserDao;

    @Autowired
    private MdlPmbEnrollmentDao mdlPmbEnrollmentDao;

    @Autowired
    private MdlUserEnrolmentsDao mdlUserEnrolmentsDao;

    @Autowired
    private MdlRoleAssignmentsDao mdlRoleAssignmentsDao;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @PostMapping("/api/pmbelearning")
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @ResponseBody
    public void prosesPmb(@RequestBody List<PmbEnrollDto> pmbenroll){
//        List<MdlUserDto> daftarUser = getUserPmb();

            for(PmbEnrollDto pme : pmbenroll){
                    System.out.println(pme.getNama());
                    System.out.println("PROGRESS ADD NEW USER : START");
                    String password = pme.getNomor();

//            Iterable<MdlUser> mdlUser = mdlUserDao.findAll();
                    MdlUser mdlUser1 = mdlUserDao.findByIdnumber(pme.getNomor());



                    if (mdlUser1 == null) {
                            //input user baru
                            MdlUser mu = new MdlUser();

                            mu.setId(new BigInteger(String.valueOf(mdlUserDao.cariMaxId() + 1)));
                            mu.setAuth("manual");
                            mu.setConfirmed(1);
                            mu.setPolicyagreed(0);
                            mu.setDeleted(0);
                            mu.setSuspended(0);
                            mu.setMnethostid(BigInteger.valueOf(1));
                            mu.setUsername(pme.getNomor());
                            mu.setPassword(passwordEncoder.encode(password));
                            mu.setIdnumber(pme.getNomor());
                            mu.setFirstname(pme.getNama());
                            mu.setLastname("-");
                            mu.setEmail(pme.getEmail());
                            mu.setEmailstop(0);
                            mu.setIcq("");
                            mu.setSkype("");
                            mu.setYahoo("");
                            mu.setAim("");
                            mu.setMsn("");
                            mu.setPhone1(pme.getNoHp());
                            mu.setPhone2("");
                            mu.setInstitution("");
                            mu.setDepartment("");
                            mu.setAddress("");
                            mu.setCity("");
                            mu.setCountry("ID");
                            mu.setLang("en");
                            mu.setCalendartype("gregorian");
                            mu.setTheme("");
                            mu.setTimezone("99");
                            mu.setFirstaccess(BigInteger.valueOf(0));
                            mu.setLastaccess(BigInteger.valueOf(0));
                            mu.setLastlogin(BigInteger.valueOf(0));
                            mu.setCurrentlogin(BigInteger.valueOf(0));
                            mu.setLastip("");
                            mu.setSecret("");
                            mu.setPicture(BigInteger.valueOf(0));
                            mu.setUrl("");
                            mu.setDescription("");
                            mu.setDescriptionformat(1);
                            mu.setMailformat(1);
                            mu.setMaildigest(0);
                            mu.setMaildisplay(2);
                            mu.setAutosubscribe(1);
                            mu.setTrackforums(1);
                            mu.setTimecreated(BigInteger.valueOf(0));
                            mu.setTimemodified(BigInteger.valueOf(0));
                            mu.setTrustbitmask(BigInteger.valueOf(0));
                            mu.setImagealt("");
                            mu.setLastnamephonetic("");
                            mu.setFirstnamephonetic("");
                            mu.setMiddlename("");
                            mu.setAlternatename("");
                            mu.setMoodlenetprofile("");
                            mdlUserDao.save(mu);

                    }

                    System.out.println("PROGRESS ADD NEW USER : FINISH");

                    //temporary table
                    System.out.println("PROGRESS ADD TEMP TABLE : START");
                    MdlPmbEnrollment mpe = new MdlPmbEnrollment();

                    mpe.setIdMahasiswa(pme.getNomor());
                    mpe.setEmail(pme.getEmail());
                    mpe.setIdNumberElearning(pme.getIdElearning());
                    mdlPmbEnrollmentDao.save(mpe);
                    System.out.println("PROGRESS ADD TEMP TABLE : FINISH");


                    //enrollment
                    System.out.println("PROGRESS ADD USER ENROL : START");
                    mdlUserEnrolmentsDao.userEnroll();
                    System.out.println("PROGRESS ADD USER ENROL : FINISH");

                    System.out.println("PROGRESS ADD ROLE ENROL : START");
                    mdlRoleAssignmentsDao.roleEnroll();
                    System.out.println("PROGRESS ADD ROLE ENROL : FINISH");


                    //fitur delete mdl_pmb_enrollment


            }


    }



}
