package com.tazkia.moodlesmile.entity;


import lombok.Data;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Data
@Entity
public class MdlKrsSmile {

    @Id
    private String idKrsDetail;

    private String idKrs;

    private String idNumberElearning;

    private String idJadwal;

    private String idMahasiswa;

    private String email;

    private String nim;

}
