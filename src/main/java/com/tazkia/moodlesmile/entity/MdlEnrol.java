package com.tazkia.moodlesmile.entity;

import lombok.Data;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import java.math.BigInteger;

@Data
@Entity
public class MdlEnrol {

    @Id
    private BigInteger id;
}
