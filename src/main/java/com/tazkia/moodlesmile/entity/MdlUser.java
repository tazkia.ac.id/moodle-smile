package com.tazkia.moodlesmile.entity;

import com.sun.istack.NotNull;
import lombok.Data;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import java.math.BigInteger;

@Data
@Entity
public class MdlUser {

    @Id
    private BigInteger id;

    
    private String auth;

    
    @Column(columnDefinition = "TINYINT")
    private Integer confirmed;

    
    @Column(columnDefinition = "TINYINT")
    private Integer policyagreed;

    
    @Column(columnDefinition = "TINYINT")
    private Integer deleted;

    
    @Column(columnDefinition = "TINYINT")
    private Integer suspended;

    
    private BigInteger mnethostid;

    
    private String username;

    
    private String password;

    
    private String idnumber;

    
    private String firstname;

    private String lastname;


    private String email;


    @Column(columnDefinition = "TINYINT")
    private Integer emailstop;


    private String icq;


    private String skype;


    private String yahoo;


    private String aim;


    private String msn;


    private String phone1;


    private String phone2;


    private String institution;


    private String department;


    private String address;


    private String city;


    private String country;


    private String lang;


    private String calendartype;


    private String theme;


    private String timezone;


    private BigInteger firstaccess;


    private BigInteger lastaccess;


    private BigInteger lastlogin;


    private BigInteger currentlogin;


    private String lastip;


    private String secret;


    private BigInteger picture;


    private String url;

    @Column(columnDefinition = "LONGTEXT") @Lob
    private String description;


    @Column(columnDefinition = "TINYINT")
    private Integer descriptionformat;


    @Column(columnDefinition = "TINYINT")
    private Integer mailformat;


    @Column(columnDefinition = "TINYINT")
    private Integer maildigest;


    @Column(columnDefinition = "TINYINT")
    private Integer maildisplay;


    @Column(columnDefinition = "TINYINT")
    private Integer autosubscribe;


    @Column(columnDefinition = "TINYINT")
    private Integer trackforums;


    private BigInteger timecreated;


    private BigInteger timemodified;


    private BigInteger trustbitmask;

    private String imagealt;

    private String lastnamephonetic;

    private String firstnamephonetic;

    private String middlename;

    private String alternatename;

    private String moodlenetprofile;





}
