package com.tazkia.moodlesmile.entity;

import lombok.Data;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import java.math.BigInteger;

@Data
@Entity
public class MdlRoleAssignments {

    @Id
    private BigInteger id;

    private BigInteger roleid;

    private BigInteger contextid;

    private BigInteger userid;

    private BigInteger timemodified;

    private BigInteger modifierid;

    private String component;

    private BigInteger itemid;

    private BigInteger sortorder;

    private String statusImport;

}
