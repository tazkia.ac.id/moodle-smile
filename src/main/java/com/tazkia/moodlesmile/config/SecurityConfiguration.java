package com.tazkia.moodlesmile.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled=true,proxyTargetClass=true)
@EnableMethodSecurity(securedEnabled = true, jsr250Enabled = true)
public class SecurityConfiguration {

//    @Override
//    public void configure(HttpSecurity http) throws Exception {
//        http.csrf().disable().authorizeRequests()
//                .antMatchers("/").permitAll()
//                .antMatchers(HttpMethod.POST,"/api/pmbelearning").permitAll()
//                .antMatchers(HttpMethod.GET,"/api/bobottugas").permitAll()
//                .antMatchers(HttpMethod.GET,"/api/bobotaslitugas").permitAll()
//                .antMatchers(HttpMethod.GET,"/api/bobotuts").permitAll()
//                .antMatchers(HttpMethod.GET,"/api/bobotuas").permitAll()
//                .antMatchers(HttpMethod.GET,"/api/nilaitugas").permitAll()
//                .antMatchers(HttpMethod.GET,"/api/nilaicounttugas").permitAll()
//                .antMatchers(HttpMethod.GET,"/api/nilaiuts").permitAll()
//                .antMatchers(HttpMethod.GET,"/api/nilaiuas").permitAll()
//                .antMatchers(HttpMethod.GET,"/api/nilaitugas2").permitAll()
//                .antMatchers(HttpMethod.GET,"/api/nilaiuts2").permitAll()
//                .antMatchers(HttpMethod.GET,"/api/nilaiuas2").permitAll()
//                .antMatchers(HttpMethod.GET,"/api/nilaipresensi").permitAll()
//                .antMatchers(HttpMethod.GET,"/api/nilaitugaspermhs").permitAll()
//                .antMatchers(HttpMethod.GET,"/api/nilaiutspermhs").permitAll()
//                .antMatchers(HttpMethod.GET,"/api/nilaiuaspermhs").permitAll()
//                .antMatchers(HttpMethod.GET,"/api/sessiondosen").permitAll()
//                .antMatchers(HttpMethod.GET,"/api/sessiondosen2").permitAll()
//                .antMatchers(HttpMethod.GET,"/api/sessionmahasiswa").permitAll()
//                .antMatchers(HttpMethod.GET,"/api/sessionmahasiswa2").permitAll()
//                .antMatchers(HttpMethod.GET,"/api/sessiondosen3").permitAll()
//                .anyRequest().authenticated();
//    }



    @Bean @Order(2)
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(auth -> auth
                        .requestMatchers("/",
                                "/api/pmbelearning",
                                "/api/bobottugas",
                                "/api/bobotaslitugas",
                                "/api/bobotuts",
                                "/api/bobotuas",
                                "/api/nilaitugas",
                                "/api/nilaicounttugas",
                                "/api/nilaiuts",
                                "/api/nilaiuas",
                                "/api/nilaitugas2",
                                "/api/nilaiuts2",
                                "/api/nilaiuas2",
                                "/api/nilaipresensi",
                                "/api/nilaitugaspermhs",
                                "/api/nilaiutspermhs",
                                "/api/nilaiuaspermhs",
                                "/api/sessiondosen",
                                "/api/sessiondosen2",
                                "/api/sessionmahasiswa",
                                "/api/sessionmahasiswa2",
                                "/api/sessiondosen3")
                .permitAll()
                .anyRequest().authenticated());
        return http.build();
    }



    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
